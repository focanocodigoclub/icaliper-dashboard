(function () {
  'use strict';

  var express              = require('express');
  var routes               = express.Router();
  var fs                   = require('fs');
  var path                 = require('path');
  var inspectionController = require('../inspection/controller')


  routes.get('/login',Login); // Renderiza a pagina de login
  routes.post('/login',Authentic); // Autenticas o usuário.
  routes.get('/logout',Logout);
  routes.get('/inspection/list',listInspection);
  routes.get('/inspection/:id',viewInspection);


  function Login(req,resp) {
      resp.render('login')
  }
  function Logout(req,resp) {
    req.session = null;
    resp.redirect('./login');
  }
  function Authentic(req,resp) {

      var auth = req.body;

      if(auth.username != "msiforks") { return resp.redirect(401,'/msiforks/login') ;}
      if(auth.password != "icaliper2.0") { return resp.redirect(401,'/msiforks/login') ;}

      req.session.logged = true;

      resp.redirect('./inspection/list');

  }

  function listInspection(req,resp) {

      if(!req.session.logged){
        return resp.redirect(401,'/msiforks/login');
      }

      resp.render('./icaliper/list-inspection');
  }

  function viewInspection(req,resp) {


      var data = req.params;

      var pdfGenerate = data.id.indexOf('-pdf') > 0 ? true : false;
          data.id = data.id.replace('-pdf','');

	    console.log(pdfGenerate);
      if(!pdfGenerate){

         var ICtrl = require('../inspection/controller'); // Controladora da inspeção
             ICtrl.generatePDF({ id : data.id })  // Gera o PDF já na pasta

      }

      if(!req.session.logged && !pdfGenerate){
          return resp.redirect(401,'/msiforks/login');
      }


      inspectionController.getInspectionForId(data,function (err,data) {
          var result = { data : data.data[0] } // pegando a inspeção retornada.
          resp.render('./icaliper/inspection',result);
      })

  }

  module.exports = routes;
}());
