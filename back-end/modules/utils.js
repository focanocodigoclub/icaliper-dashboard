(function () {

  'use strict';

  var crypto = require('crypto')
  var jwt = require('jsonwebtoken');
  var Promise = require("es6-promise").Promise;
  var fs = require('fs');
  var path = require('path');
  var mkdirp = require('mkdirp');

  // var http = require('http');
  var nodemailer = require('nodemailer');


  var dotenv = require('dotenv');
  dotenv.load();
  var obj = {};


  obj.generatePassword = function (password) {

    return new Promise(function (resolve, reject) {
      try {
        var encrypt = '*' + process.env.SECRETKEY;
        encrypt += password.toString();
        password = crypto
          .createHash('MD5')
          .update(encrypt)
          .digest('base64');
        resolve(password);
      } catch (e) {
        reject(e);
      }
    });

  };

  obj.generateToken = function (user) {

    return new Promise(function (resolve, reject) {
      try {
        var secret = process.env.SECRETKEY;
        var options = { expiresIn: (1440 * 60) }; // 1440 minutes or 24h
        var token = jwt.sign(user, secret, options);
        resolve(token);
      } catch (e) {
        reject(e);
      }
    });

  };

  obj.validToken = function (token) {
    return new Promise(function (resolve, reject) {
      jwt.verify(token, process.env.SECRETKEY, function (err, decode) {
        if (err) { reject("Token inválido"); return; }
        resolve(decode);
      });
    });
  };

  obj.uploadFile = function (file, config) {
    return new Promise(function (resolve, reject) {

      if (config.name === undefined) { reject("arquivo sem nome"); return; }
      if (config.path === undefined) { reject("Um caminho é obrigatório"); return; }

      var url = path.join(__dirname, '../../front-end/');
      url += config.path;
      url = path.normalize(url);
      console.log(file);

      fs.readFile(file.path, function (err, data) {
        if (err) {
          console.log("Erro -" + err);
          reject(err);
          return;
        }
        var upload = function () {
          var newPath = url + '/' + config.name + '.' + file.type.split('/')[1];
          fs.writeFile(newPath, data, function (err) {
            if (err) {
              reject(err);
              return;
            }
            resolve(newPath);
            console.log("Arquivo Salvo com Sucesso!");
          });
        }
        if (!fs.existsSync(url)) {
          mkdirp(url, function (err) {
            if (err) { console.error(err); }
            else { upload(); }
          });
        } else { upload(); }

      });
    });
  };


  /*
  
    To = vem direto do arquivo de variaveis, Junto com as credenciais do sistema.
  
    From = Para onde você quer enviar
    Subject = Assunto
    html =  Corpo do e-mail
  
  */
  obj.SendEmail = function (from, to, subject, html, file) {

    return new Promise(function (resolve, reject) {

      // create reusable transporter object using the default SMTP transport
      let transporter = nodemailer.createTransport({
        host: process.env.MAIL_HOST,
        port: process.env.MAIL_PORT,
        secure: true, // use TLS
        auth: {
          user: process.env.MAIL_USER, // Your email id
          pass: process.env.MAIL_PASS // Your password
        },
        tls: {
          rejectUnauthorized: false
        }
      });
      console.log('[ok] sendmail ---\n');
      transporter.verify(function (error, success) {
        if (error) {
          console.log(JSON.stringify(error));
        } else {
          console.log('Server is ready to take our messages');
        }
      });

      // Documentação
      // https://github.com/sendgrid/sendgrid-nodejs

      from = from || process.env.MAIL_USER;
      to = to;

      // setup email data with unicode symbols
      let mailOptions = {
        from: from, // sender address
        to: '', // list of receivers
        subject: subject, // Subject line
        html: html // html body
      };

      to.forEach(function (t) {
        mailOptions.to += t + (to.length > 1 ? "," : '');
      })

      if (file) {
        mailOptions.attachment = [{ path: file.path, filename: file.name }];
      }

      console.log(mailOptions)

      transporter.sendMail(mailOptions, callback);

      function callback(err, response) {

        if (err) {
          console.log(JSON.stringify(err));
          reject(err);
        }
        else {
          console.log("Message sent: ");
          console.log("-------------------------");
          console.log("Accepted: " + response.accepted);
          console.log("Rejected: " + response.rejected);
          console.log("Response: " + response.response);
          console.log("Envelope: " + JSON.stringify(response.envelope));
          console.log("MessageId: " + response.messageId);
          resolve(response);
        }
      }


    })

  };



  module.exports = obj;
})();
