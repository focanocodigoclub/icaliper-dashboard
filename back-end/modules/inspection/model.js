(function () {
    'use strict';
    var db = require('../../database/database').load();
    var model = {}


    model.Submit = Submit;
    model.HistoricForUser = HistoricForUser;
    model.HistoricList = HistoricList
    model.getInspectionForId = getInspectionForId;

    function Submit(data, cb) {

        var insert_fork_historic = data.forks_inspection;
        delete data.forks_inspection;
        var insert = data;
        
        var q = db.query('insert into historic_inspection set ?', insert, cbQuery);

        function cbQuery(err, result) {

            if (err) {
                console.log(JSON.stringify(err));
                return cb({ code: 500, msg: 'Ocorreu um erro no sistema, aguarde a normalização e tente novamente.' });
            }

            insert_fork_historic.historic_code = result.insertId;

            q = db.query('insert into forks_inspection set ?', insert_fork_historic, (err, result) => {
                result.historic_code = insert_fork_historic.historic_code;
                cbQueryFork(err, result);
            });



            return;

        }




        function cbQueryFork(err, result) {

            if (err) {
                console.log(JSON.stringify(err));
                return cb({ code: 500, msg: 'Ocorreu um erro no sistema, aguarde a normalização e tente novamente.' });
            }
            return cb(null, {
                msg: "Inspeção Cadastrada com sucesso", data: {
                    historic_code: result.historic_code
                }
            })
        }
    }


    function HistoricForUser(data, cb) {

        var query = { 'hi.user_code': data.id };
        var q = db.query('select * from historic_inspection hi inner join forks_inspection fi on ( fi.historic_code = hi.historic_code ) where ?', query, cbQuery);

        function cbQuery(err, result) {


            if (err) {
                console.info('err', err);
                return cb({ code: 500, msg: 'Ocorreu um erro no sistema, aguarde a normalização e tente novamente.' });
            }


            return cb(null, { msg: "", data: result })

        }
    }

    function HistoricList(cb) {

        var query = {};

        var sql = `
    select hi.historic_code,
     hi.lote ,
     hi.date_inspection ,
     
     hi.thickness_average ,
     
      hi.unit_measure,
     hi.language_inspection ,
     
     hi.status_crack ,
     hi.analysis_parts ,
     hi.step2_obs ,
     hi.step5_obs ,
     hi.final_obs ,
    fi.fork_code ,
    fi.name as forkname,
    fi.length ,
    fi.width ,
    fi.thickness ,
    us.user_code ,
    us.name as inspector_name,
    us.email ,
    us.phone ,
    us.company
    `;
        sql += ' from historic_inspection hi '
        sql += ' inner join forks_inspection fi on ( fi.historic_code = hi.historic_code ) ';
        sql += ' inner join user us on ( hi.user_code = us.user_code )';


        var q = db.query(sql, query, cbQuery);



        function cbQuery(err, result) {


            if (err) {
                console.info('err', err);
                return cb({ code: 500, msg: 'Ocorreu um erro no sistema, aguarde a normalização e tente novamente.' });
            }
            return cb(null, { msg: "", data: result })
        }
    }

    function getInspectionForId(data, cb) {

        var query = { "hi.historic_code": data.id };

        var sql = ` select    hi.historic_code
                        , hi.lote
                        , hi.date_inspection
                        , hi.thickness_average
                        , hi.language_inspection
                        , hi.unit_measure
                        , hi.status_crack
                        , hi.analysis_parts
                        , hi.step2_obs, hi.step5_obs
                        , hi.final_obs, fi.fork_code, fi.name as forkname
                        , fi.length, fi.width, fi.thickness, us.user_code
                        , us.name as inspector_name, us.email, us.phone
                        , us.company
                        from historic_inspection hi
                        inner join forks_inspection fi on ( fi.historic_code = hi.historic_code )
                        inner join user us on ( hi.user_code = us.user_code )
                        where ?`;

        var q = db.query(sql, query, cbQuery);

        function cbQuery(err, result) {
            if (err) {
                console.info('err', err);
                return cb({ code: 500, msg: 'Ocorreu um erro no sistema, aguarde a normalização e tente novamente.' });
            }
            return cb(null, { msg: "", data: result })
        }

    }

    module.exports = model;
}());
