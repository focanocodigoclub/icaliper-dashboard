(function () {
  'use strict';

  var express = require('express');
  var routes = express.Router();

  var controller = require('./controller')


  routes.post('/submit', Submit);
  routes.post('/:id/send-to-email', SendToEmail);

  routes.get('/historic/user/:id', HistoricForUser);
  routes.get('/historic/list', HistoricList);
  routes.get('/:id', getInspectionForId);
  routes.get('/:id/generate-pdf', generatePDF);


  var ResponseCB = function ResponseCB(err, result, resp) {
    if (err) {
      resp.status(err.code);
      return resp.json({ success: false, data: null, msg: err.msg });
    }
    return resp.json({ success: true, data: result.data, msg: result.msg });
  }

  function Submit(req, resp) {
    
    var data = req.body;

    controller.Submit(data, function (err, result) {
      ResponseCB(err, result, resp)
    });

  }

  function HistoricList(req, resp) {
    controller.HistoricList(function (err, result) {
      ResponseCB(err, result, resp)
    })
  }

  function HistoricForUser(req, resp) {
    var data = req.params;

    controller.HistoricForUser(data, function (err, result) {
      ResponseCB(err, result, resp)
    })
  }

  function getInspectionForId(req, resp) {
    var data = req.params;
    controller.getInspectionForId(data, function (err, result) {
      ResponseCB(err, result, resp)
    })
  }

  function generatePDF(req, resp) {
    var data = req.params;
    console.log("------ orrah");
    controller.generatePDF(data, function (err, result) {
      ResponseCB(err, result, resp)
    })
  };

  function SendToEmail(req, resp) {
    var data = {
      email: req.body.email
      , id: req.params.id
    };
    console.log(data)
    controller.SendToEmail(data, function (err, result) {
      ResponseCB(err, result, resp)
    })
  }

  module.exports = routes;
}());
