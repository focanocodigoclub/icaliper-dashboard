(function () {
    'use strict';

    var path = require('path');
    var fs = require('fs');
    var jade = require('jade');
    var phantom = require('phantom');
    var Model = require('./model');
    var Utils = require('../utils.js');
    var controller = {};


    controller.Submit = Submit;
    controller.HistoricForUser = HistoricForUser;
    controller.HistoricList = HistoricList;
    controller.generatePDF = generatePDF;
    controller.getInspectionForId = getInspectionForId;
    controller.SendToEmail = SendToEmail;



    function Submit(data, cb) {


        if (!data.user_code) {
            return cb({ code: 400, msg: "É obrigatório ser um usuário." })
        }

        if (!data.language_inspection) {
            data.language_inspection = "pt-br";
            // Caso não tenha definido a linguagem, coloca como pt-br...
        }

        if (data.model == 0) {
            return cb({ code: 400, msg: "Selecione um modelo de Garfo." })
        }


        // if(!data.machine){
        //   return cb({code:400, msg:"Insira o código identificador da maquina."})
        // }

        if (!data.lote) {
            return cb({ code: 400, msg: "Insira o numero do lote." })
        }


        /*
        Espessura Original deve estar entre 10 e 1000 e não pode ser vázia
        */
        if (10 > data.thickness_original || data.thickness_original > 1000 || !data.thickness_original) {
            return cb({ code: 400, msg: "Espessura Original deve estar entre 10 e 1000 e não pode ser vázia" })
        }

        /*
        Comprimento Original deve estar entre 10 e 3000 e não pode ser vázia
        */
        if (10 > data.length_original || data.length_original > 3000 || !data.length_original) {
            return cb({ code: 400, msg: "Comprimento Original deve estar entre 10 e 3000 e não pode ser vázia" })
        }

        /*
        Largura Original deve estar entre 10 e 300 e não pode ser vázia
        */
        if (10 > data.width_original || data.width_original > 300 || !data.width_original) {
            return cb({ code: 400, msg: "Largura Original deve estar entre 10 e 300 e não pode ser vázia" })
        }


        /*
        Desgaste da espessura Medida deve estar entre 10 e 300 e não pode ser vázia
        */
        if (10 > data.thickness_average || data.thickness_average > 300 || !data.thickness_average) {
            return cb({ code: 400, msg: "Espessura Medida deve estar entre 10 e 300 e não pode ser vázia" });
        }

        /*
        Comprimento medido deve estar entre 0 e 1000 e não pode ser vázio
        */
        // if(-1 > data.length_average || data.length_average >= 1000 || data.length_average == undefined ){
        //   return cb({code:400, msg:"Comprimento medido deve estar entre 0 e 1000 e não pode ser vázio"});
        // }

        data.date_inspection = new Date();

        /*
          Caso o modelo seja
        */
        if (data.modelcustom == "" && data.model == 1) {
            return cb({ code: 400, msg: "Informe o nome do modelo do Garfo " });
        }


        /*
          Valor da Hipotenusa medida deve estar entre 300 e 3000 e não pode ser vázio
        */
        // if( 559 > data.hypotenuse_average || data.hypotenuse_average > 3000 || !data.hypotenuse_average){
        //   return cb({code:400, msg:"Valor da Hipotenusa medida deve estar entre 560 e 3000 e não pode ser vázio"});
        // }



        /*
    
          Esse valor hoje está setado em 400 fixos
          Valor do Cateto medido deve estar entre 200 e 2000 e não pode ser vázio
    
        */
        // if( 200 > data.cathetus || data.cathetus > 2000 || !data.cathetus ){
        //   return cb({code:400, msg:"Valor do Cateto medido deve estar entre 200 e 2000 e não pode ser vázio"});
        // }


        if (data.step2_obs.length > 250) {
            return cb({ code: 400, msg: "Todas as observações devem possuir até 250 caracteres" });
        }
        if (data.step5_obs.length > 250) {

            return cb({ code: 400, msg: "Todas as observações devem possuir até 250 caracteres" });

        }

        if (data.final_obs.length > 250) {

            return cb({ code: 400, msg: "Observação final devem possuir até 250 caracteres" });

        }

        if (data.analysis_parts) {
            data.analysis_parts = data.analysis_parts.join(',');
        } else {
            data.analysis_parts = "";
        }




        var forks_inspection = {
            user_code: data.user_code,
            fork_code: data.model,
            name: data.modelcustom,
            thickness: data.thickness_original,
            length: data.length_original,
            width: data.width_original
        }

        var email_user = data.email_user;

        delete data.model;
        delete data.modelcustom;
        delete data.thickness_original;
        delete data.length_original;
        delete data.length_average; // Analisar isso 
        delete data.width_original;
        delete data.cathetus;
        delete data.email_user;

        delete data.machine;
        delete data.producer;
        delete data.hypotenuse_average;
        delete data.hypotenuse_angle;

        data.forks_inspection = forks_inspection;
        return Model.Submit(data, (err, result) => {

            console.info(JSON.stringify(result));

            var data = { id: result.data.historic_code };

            cb(err, result);

            generatePDF(data).then((err, result) => {


                var variables = {
                    date: (new Date()).toLocaleDateString()
                    , linkPDF: process.env.URL_SERVER + "/inspection-pdf/" + data.id + "-inspection.pdf"
                }

                var html = jade.renderFile(__dirname + '/templates-email/submit-inspection.jade', variables);
                var title = "MSI - Inspeção Nº" + data.id;

                // var email = [
                //     "jacqueline.gomes@msiforks.com.br"
                //     , "rafael.santana@msiforks.com.br"
                //     , "clima@msiforks.com.br"
                // ];

                var email = [email_user];
                //email.push(email_user); // e-mail do usuário que fez a inspeção.


                var file = {
                    path: "./front-end/inspection-pdf/" + data.id + "-inspection.pdf"
                    , name: data.id + "-inspection.pdf"
                }

                console.info('Preparando para enviar e-mail');

                Utils.SendEmail(null, email, title, html, file).then((sucess) => {
                    console.info("E-mail enviado com sucesso......")
                }, (err) => {
                    console.info('Erro no PDF :(', err);
                })

            });
        });
    }

    function HistoricList(cb) {
        Model.HistoricList(cb)
    }

    function HistoricForUser(data, cb) {
        Model.HistoricForUser(data, cb);
    }

    function getInspectionForId(data, cb) {
        Model.getInspectionForId(data, cb)
    }


    function generatePDF(data) {

        return new Promise((resolve, reject) => {

            try {

                console.log(`
            Aqui , ${JSON.stringify(data)}
        `);


                if (!data.id) {
                    return reject();
                }

                var dirPDF = path.join(__dirname, '../../../front-end/inspection-pdf/' + data.id + '-inspection.pdf');



                fs.exists(dirPDF, (exists) => {

                    if (!exists) {

                        var ph, page;
                        var url = process.env.URL_SERVER + "/msiforks/inspection/" + data.id + "-pdf";
                        console.log(url);
                        phantom.create()
                            .then(function (ph) {
                                ph.createPage()
                                    .then(function (page) {
                                        page.property('viewportSize', { width: 1000, height: 600 })
                                        page.property('paperSize', "A4")
                                        page.open(url)
                                            .then(function (status) {
                                                var title = data.id + '-inspection.pdf';
                                                page.render('./front-end/inspection-pdf/' + title).then(function () {
                                                    ph.exit();
                                                    console.log('ok');
                                                    resolve(null, { data: status });
                                                });
                                            });
                                    });
                            }, (err) => {
                                console.log(err);
                                reject(err);
                            });

                    } else {

                        resolve(null, { data: exists });

                    }

                });

            } catch (err) {

                reject();

            }

        });
        // if(!data.enabled) { return cb({code:401, msg:"Você não possui permissão"})  }
    };

    function SendToEmail(data, cb) {

        if (!data.id) { return cb({ code: 400, msg: "Informe a inspeção que deseja.." }); }
        if (!data.email) { return cb({ code: 400, msg: "E-mail não informado." }); }

        generatePDF(data).then((err, result) => {
            console.info("PDF Gerado com sucesso ......")
            var variables = {
                date: (new Date()).toLocaleDateString()
                , linkPDF: process.env.URL_SERVER + "/inspection-pdf/" + data.id + "-inspection.pdf"
            }

            var html = jade.renderFile(__dirname + '/templates-email/submit-inspection.jade', variables);
            var title = "MSI - Inspeção Nº" + data.id;

            var email = [data.email];

            var file = {
                path: "./front-end/inspection-pdf/" + data.id + "-inspection.pdf"
                , name: data.id + "-inspection.pdf"
            }

            console.info('Preparando para enviar e-mail');

            Utils.SendEmail(null, email, title, html, file).then((sucess) => {
                return cb(null, { msg: "E-mail enviado com sucesso......", data: {} })
            }, (err) => {
                console.log(err);
                return cb({ code: 500, msg: 'Ocorreu um erro no sistema, aguarde a normalização e tente novamente.' });
            })
        });

    }

    module.exports = controller;
}());
