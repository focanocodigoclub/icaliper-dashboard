(function () {
  'use strict';
  var db = require('../../database/database').load();
  var model = {}


  model.Login = Login;
  model.Signup = Signup;
  model.verifyEmail = verifyEmail;
  model.Edit = Edit
  model.ValidPassword = ValidPassword;
  model.RecoveryToken = RecoveryToken;


  function ValidPassword(user) {

    return new Promise(function (resolve, reject) {

      var query = [{ user_code: user.user_code }, { password: user.password }];

      var q = db.query('select count(user_code) as total from user where ? and ?', query, function (err, result) {
        if (err) {
          reject(err);
          return;
        }
        if (result[0].total == 0) {
          resolve(false);
        } else {
          resolve(true);
        }

      })
    });

  };

  function verifyEmail(email) {
    return new Promise(function (resolve, reject) {
      db.query('select count(email) as total from user where ?', { email: email }, function (err, result) {
        console.log(err, result)
        if (err) {
          reject(err);
          return;
        }
        resolve(result[0])
      })
    });
  }



  function Signup(data, cb) {


    return verifyEmail(data.email).then(ValidEmail, InvalidEmail)

    function ValidEmail(count) {

      if (count.total > 0) {
        cb({ code: 400, msg: 'E-mail já existente, tente com outro endereço' })
        return;
      }

      var query = data;
      var q = db.query('insert into user set ?', query, cbQuery);


    }

    function InvalidEmail(err) {
      return cb({ code: 500, msg: 'Ocorreu um erro no sistema, aguarde a normalização e tente novamente.' });
    }



    function cbQuery(err, result) {


      if (err) {
        return cb({ code: 500, msg: 'Ocorreu um erro no sistema, aguarde a normalização e tente novamente.' });
      }

      data.user_code = result.insertId;

      return cb(null, { msg: "", data: data })
    }
  }


  function Login(data, cb) {

    var query = [{ email: data.email }, { password: data.password }]

    var q = db.query('select * from user where ? and ?', query, cbQuery)



    function cbQuery(err, result) {

      if (err) {
        console.log(err);
        return cb({ code: 500, msg: 'Ocorreu um erro no sistema, aguarde a normalização e tente novamente.' });
      }
      if (result.length === 0) {
        return cb({ code: 404, msg: 'Usuário não encontrado, ou desativado.' });
      }

      if (result.length > 1) {
        return cb({ code: 500, msg: 'Houve um problema ao tentar acessar sua conta, entre em contato com o suporte por favor.' });
      }

      delete result[0].password;

      return cb(null, { msg: "", data: result[0] })
    }
  }

  function Edit(update, cb) {

    var query = { user_code: update.user_code }
    delete update.user_code
    var q = db.query('update user set  ? where ?', [update, query], cbQuery)

    function cbQuery(err, result) {
      if (err) {
        return cb({ code: 500, msg: 'Ocorreu um erro no sistema, aguarde a normalização e tente novamente.' });
      }
      return cb(null, { msg: "Usuário Atualizado com sucesso.", data: {} })
    }
  }

  function RecoveryToken(data, cb) {
    var query = { email: data.email }
    db.query('select user_code, token_recovery from user where ?', query, function (err, result) {
      if (err) {
        return cb({ code: 500, msg: 'Ocorreu um erro no sistema, aguarde a normalização e tente novamente.' });
      }
      if (result.length === 0) {
        return cb({ code: 404, msg: 'Usuário não encontrado, ou desativado.' });
      }
      return cb(null, result[0])

    })
  };
  module.exports = model;
}());
