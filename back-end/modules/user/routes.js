(function () {
  'use strict';

  var express = require('express');
  var routes = express.Router();

  var controller = require('./controller')


  routes.post('/login', Login);
  routes.post('/signup', Signup);
  routes.put('/edit', Edit);
  routes.put('/change-password', ChangePassword);
  routes.post('/recovery-password', RecoveryPassword)

  /*

    Função de Callback padrão.
    Parametros 
    @err : Caso exista erro, se não existir o valor é Null
    @result : Resultado que será enviado de resposta
    @resp : Função do Express, é necessário enviar por parametro ela, para que seja possível enviar uma
    resposta do servidor
  */
  var ResponseCB = function ResponseCB(err, result, resp) {

    if (err) {
      resp.status(err.code);
      return resp.json({ success: false, data: null, msg: err.msg });
    }

    return resp.json({ success: true, data: result.data, msg: result.msg });

  }

  /*
    Função para Registro
    Body da Resquisição deve conter esses valores

    name
    company
    // phone
    email
    password
    

  */
  function Signup(req, resp) {

    console.log("SIGNUP", req.body)

    var data = req.body;

    controller.Signup(data, function (err, result) {
      ResponseCB(err, result, resp)
    })


  }


  function Login(req, resp) {

    var data = req.body

    controller.Login(data, function (err, result) {
      ResponseCB(err, result, resp)
    })

  }

  function Edit(req, resp) {

    var update = req.body
    console.info('update', update);
    controller.Edit(update, function (err, result) {
      ResponseCB(err, result, resp)
    })
  }


  function ChangePassword(req, resp) {
    var update = req.body

    controller.ChangePassword(update, function (err, result) {
      ResponseCB(err, result, resp)
    })
  }

  function RecoveryPassword(req, resp) {

    var data = req.body

    controller.RecoveryPassword(data, function (err, result) {
      ResponseCB(err, result, resp)
    })
  }

  module.exports = routes;
}());
