(function () {
  'use strict';

  var jade = require('jade');
  var Model = require('./model');
  var Utils = require('../utils');
  var controller = {};


  controller.Login = Login;
  controller.Signup = Signup;
  controller.Edit = Edit;
  controller.ChangePassword = ChangePassword;
  controller.RecoveryPassword = RecoveryPassword;

  function Signup(data, cb) {


    if (!data.name) {

      return cb({ code: 400, msg: 'Nome é obrigatório.' });

    }
    if (!data.company) {

      return cb({ code: 400, msg: 'Empresa é obrigatório.' });

    }

    // if(!data.phone) {
    //   return cb({code : 400 , msg: 'Telefone é obrigatório.'});
    // }

    if (!data.email) {

      return cb({ code: 400, msg: 'E-mail é obrigatório.' });

    }
    if (!data.password) {

      return cb({ code: 400, msg: 'Senha é obrigatório.' });

    }
    if (data.password.length < 6) {

      return cb({ code: 400, msg: 'Senha deve ser maior que 6 digitos.' });

    }

    /*
      Limpa qualquer variavel que não seja valida.
    */
    for (var variable in data) {
      if (data.hasOwnProperty(variable)) {
        if (variable !== 'name' &&
          variable !== 'company' &&
          variable !== 'email' &&
          variable !== 'password') {
          delete data[variable];
        }
      }
    }



    return Utils.generatePassword(data.password).then(SuccessGeneratePassword, FailGeneratePassword)



    function SuccessGeneratePassword(password) {

      data.password = password
      var token = data.password + 'recovery-token' + (new Date().toString());

      return Utils.generatePassword(token).then(SuccessGenerateTokenRecovery, FailGenerateTokenRecovery)

    }

    function FailGeneratePassword(err) {
      return cb({ code: 500, msg: 'Ocorreu um erro no sistema, aguarde a normalização e tente novamente.' });
    }

    function SuccessGenerateTokenRecovery(token) {
      data.token_recovery = token;
      return Model.Signup(data, cb);
    }

    function FailGenerateTokenRecovery(err) {
      return cb({ code: 500, msg: 'Ocorreu um erro no sistema, aguarde a normalização e tente novamente.' });
    }
  }

  function Login(data, cb) {

    if (!data.email) {
      return cb({ code: 400, msg: 'E-mail é obrigatório' })
    }
    if (!data.password) {
      return cb({ code: 400, msg: 'Senha é obrigatória' })
    }

    Utils.generatePassword(data.password).then(SuccessGeneratePassword, FailGeneratePassword)


    function SuccessGeneratePassword(password) {

      data.password = password
      return Model.Login(data, cb);

    }

    function FailGeneratePassword(err) {
      console.log(err);
      return cb({ code: 500, msg: 'Ocorreu um erro no sistema, aguarde a normalização e tente novamente.' });
    }
  }

  function Edit(update, cb) {

    if (!update.user_code) {
      return cb({ code: 400, msg: "É obrigatório ser um usuário." })
    }

    /*
      Limpa qualquer variavel que não seja valida.
    */
    for (var variable in update) {
      if (update.hasOwnProperty(variable)) {
        if (variable !== 'user_code' &&
          variable !== 'name' &&
          variable !== 'company' &&
          variable !== 'email') {
          delete update[variable];
        }
      }
    }


    if (JSON.stringify(update) == '{}') {
      return cb({ code: 400, msg: "Não há nenhum campo para atualizar" })
    }



    return Model.Edit(update, cb);
  }


  function ChangePassword(update, cb) {

    if (!update.user_code) {
      return cb({ code: 400, msg: "É obrigatório ser um usuário." })
    }

    if (!update.password_old) {
      return cb({ code: 400, msg: "Insira a senha antiga." });
    }

    if (!update.password_new) {
      return cb({ code: 400, msg: "Insira a nova senha." });
    }

    if (update.password_new.length < 6) {

      return cb({ code: 400, msg: "A Senha nova deve ser maior que 6 digitos." });

    }
    if (update.password_new != update.password_confirm) {

      return cb({ code: 400, msg: "A Senha e confirmação de senha, não conferem." });

    }

    update.password = update.password_old;

    if (update.password) {


      return Utils.generatePassword(update.password).then(function (password) {

        update.password = password
        Model.ValidPassword(update).then(SuccessValidPassword, FailValidPassword);


      }, FailGeneratePassword)
    }



    function SuccessValidPassword(valid) {
      if (valid) {
        update.password = update.password_new;
        return Utils.generatePassword(update.password).then(SuccessGeneratePassword, FailGeneratePassword)
      }
      return cb({ code: 500, msg: 'Ocorreu um erro no sistema, aguarde a normalização e tente novamente.' });
    }

    function FailValidPassword(err) {
      return cb({ code: 500, msg: 'Ocorreu um erro no sistema, aguarde a normalização e tente novamente.' });
    }



    function SuccessGeneratePassword(password) {

      var data = {
        user_code: update.user_code,
        password: password
      }
      return Model.Edit(data, function (err, result) {
        result.msg = "Senha atualizada com sucesso."
        cb(err, result)
      });

    }

    function FailGeneratePassword(err) {
      return cb({ code: 500, msg: 'Ocorreu um erro no sistema, aguarde a normalização e tente novamente.' });
    }
  }


  /*
      Função para Gerar uma senha nova.
      Pega o e-mail passado, busca o token relacionado a ele
  */
  function RecoveryPassword(data, cb) {
    var user = data;

    if (!data.email) {
      return cb({ code: 400, msg: "E-mail inexiste." })
    }

    Model.RecoveryToken(data, function (err, result) {

      if (err) {
        return cb({ code: 500, msg: "", data: err }, null);
      }

      user.password = (Math.random() * 1e18).toString(36) // Gerando uma senha randomica.
      user.user_code = result.user_code;

      Utils.generatePassword(user.password).then(SuccessGeneratePassword, FailGeneratePassword)
    })

    function SuccessGeneratePassword(password) {
      var data = {
        user_code: user.user_code,
        password: password
      }
      return Model.Edit(data, SendEmail);
    };

    function FailGeneratePassword(err) {
      console.log(err);
      cb({ code: 500, msg: "", data: err })
    };

    function SendEmail(err, result) {

      var date = new Date();
      var variables = {
        password: user.password,
        date: date.toLocaleDateString(),
        msg: 'Foi gerada uma senha temporária, acesse o sistema em um prazo de 24h e troque sua senha. '
      }

      var html = jade.renderFile(__dirname + '/templates-email/recovery-password.jade', variables);

      Utils.SendEmail(null, [user.email], "MSI - Recuperação de Senha", html).then(SuccessSend, FailSend)


      function SuccessSend(result) {
        cb(null, { msg: "Foi enviado um e-mail com uma senha temporária para você." })
      };

      function FailSend(err) {

        cb({ code: 500, msg: "", data: err })
      };
    };
  }

  module.exports = controller;
}());
