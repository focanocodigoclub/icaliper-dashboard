(function () {
  'use strict';
  
  var express = require('express');
  var routes = express.Router();

  var controller = require('./controller')


  routes.get('/list',List);


  var ResponseCB = function ResponseCB (err,result,resp) {

       if(err){
        resp.status(err.code);
        return resp.json({success : false,data : null,msg : err.msg});
      }

      return resp.json({ success : true, data : result.data, msg : result.msg });

  }


  function List (req,resp) {

    controller.List(function  (err,result) {
      ResponseCB(err,result,resp)
    })

    
  }

  
 


  module.exports = routes;
}());