(function () {
  'use strict';
  var db = require('../../database/database').load();
  var model = {}


  model.List = List;


  function List(cb) {
    db.query('select * from forks', {}, function (err, result) {
      if (err) {
        return cb({ code: 500, msg: 'Ocorreu um erro no sistema, aguarde a normalização e tente novamente.' });
      }
      if (result.length === 0) {
        return cb({ code: 404, msg: 'Usuário não encontrado, ou desativado.' });
      }

      cb(null, { msg: "", data: result });
    })
  }







  module.exports = model;
}());