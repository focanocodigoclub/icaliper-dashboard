--
-- ER/Studio 8.0 SQL Code Generation
-- Company :      Uxer
-- Project :      database.DM1
-- Author :       Jorge
--
-- Date Created : Monday, May 02, 2016 03:35:14
-- Target DBMS : MySQL 5.x
--

--
-- TABLE: forks
--

CREATE TABLE forks(
    fork_code    INT               AUTO_INCREMENT,
    name         VARCHAR(150)      NOT NULL,
    length       DECIMAL(10, 2)    DEFAULT 0 NOT NULL,
    width        DECIMAL(10, 2)    DEFAULT 0 NOT NULL,
    thickness    DECIMAL(10, 2)    DEFAULT 0 NOT NULL,
    PRIMARY KEY (fork_code)
)ENGINE=MYISAM
;



--
-- TABLE: forks_inspection
--

CREATE TABLE forks_inspection(
    fork_code        INT               NOT NULL,
    historic_code    INT               NOT NULL,
    user_code        INT               NOT NULL,
    name             VARCHAR(150)      NOT NULL,
    length           DECIMAL(10, 2)    NOT NULL,
    width            DECIMAL(10, 2)    NOT NULL,
    thickness        DECIMAL(10, 2)    NOT NULL,
    PRIMARY KEY (fork_code, historic_code, user_code)
)ENGINE=MYISAM
;



--
-- TABLE: historic_inspection
--

CREATE TABLE historic_inspection(
    historic_code          INT               AUTO_INCREMENT,
    user_code              INT               NOT NULL,
    lote                   VARCHAR(150)      ,
    date_inspection        DATETIME          NOT NULL,
    thickness_average      DECIMAL(10, 2)    NOT NULL,
    width_average          DECIMAL(10, 2)    ,
    unit_measure            VARCHAR(45)       ,
    status_crack           INT               NOT NULL,
    analysis_parts         VARCHAR(30),
    step2_obs              TEXT,
    step5_obs              TEXT,
    final_obs              TEXT,
    language_inspection  CHAR(10)          NOT NULL,
    PRIMARY KEY (historic_code, user_code)
)ENGINE=MYISAM
;



--
-- TABLE: log
--

CREATE TABLE log(
    log_code       INT             NOT NULL,
    code           INT             NOT NULL,
    description    TEXT            NOT NULL,
    date           DATETIME        NOT NULL,
    method         VARCHAR(255)    NOT NULL,
    PRIMARY KEY (log_code)
)ENGINE=MYISAM
;



--
-- TABLE: user
--

CREATE TABLE user(
    user_code         INT             AUTO_INCREMENT,
    token_recovery    VARCHAR(50)     NOT NULL,
    name              VARCHAR(100)    NOT NULL,
    email             VARCHAR(140)    NOT NULL,
    password          VARCHAR(50)     NOT NULL,
    phone             VARCHAR(50)     NULL,
    company           VARCHAR(140)    NOT NULL,
    PRIMARY KEY (user_code)
)ENGINE=MYISAM
;



--
-- TABLE: forks_inspection
--

ALTER TABLE forks_inspection ADD CONSTRAINT Refforks6
    FOREIGN KEY (fork_code)
    REFERENCES forks(fork_code)
;

ALTER TABLE forks_inspection ADD CONSTRAINT Refhistoric_inspection7
    FOREIGN KEY (historic_code, user_code)
    REFERENCES historic_inspection(historic_code, user_code)
;


--
-- TABLE: historic_inspection
--

ALTER TABLE historic_inspection ADD CONSTRAINT Refuser5
    FOREIGN KEY (user_code)
    REFERENCES user(user_code)
;
