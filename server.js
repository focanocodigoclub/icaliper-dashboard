'use strict';

var express = require('express');
var cookieParse = require('cookie-parser');
var expressSession = require('express-session');
var cookieSession = require('cookie-session');
var bodyParser = require('body-parser');
var path = require('path');
var Middleware = require('connect-multiparty');
var dotenv = require('dotenv');
var nodeadmin = require('nodeadmin');



dotenv.load();


if (process.env.NODE_ENV === "production") {
  const utilconsole = require('util-console.log');
  utilconsole.configure({
    inject_level: true,
    log_level: "all",
    to_file: true,
    file_name: "application.log"
  });

}



var app = express();

app.set('trust proxy', 1) // trust first proxy
app.use(Middleware())
app.use(cookieParse());
app.use(nodeadmin(app));

// app.use(expressSession({ resave: true, saveUninitialized: true, secret: process.env.SECRETKEY, cookie: { secure: true, maxAge: 600000 } }));
//
app.use(cookieSession({
  name: 'session',
  keys: ['key1', 'key2']
}));

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

/*
  Configuração para renderizar arquivos .jade para HTML
  Endereço dos arquivos de views.
*/
app.set('views', __dirname + '/front-end/views/');
app.set('view engine', 'ejs');


/*
  Configuração de Rota para os arquivos estáticos do site.
*/
app.use(express.static(path.join(__dirname, '/front-end/')));



/*
Carregando Endereço, e Porta do servidor
*/
// var ipaddress = process.env.OPENSHIFT_NODEJS_IP || 'localhost';
var port = process.env.PORT || 3000;


/*
Ultima vez que o servidor foi levantando.
*/
var onlinesince = new Date();

/*
CORS
*/
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS')
  next();
});


/*
Iniciando aplicação.
*/
app.listen(port, function () {
  console.log("Server Up ------------" + onlinesince);
});


/*
Controle de API's

Seguindo o padrão

- /api/versão/modulo/

exemplo: /api/v1/user
api - apenas prefixo.
v1 - Informa a versão da API que é relacionada aquela rota
user - Módulo que aquela API acessa.

*/
var api = {};
api.user = require('./back-end/modules/user/routes');
api.forks = require('./back-end/modules/forks/routes');
api.inspection = require('./back-end/modules/inspection/routes');
api.web = require('./back-end/modules/web.admin/routes');

app.use('/api/v1/user', api.user);
app.use('/api/v1/forks', api.forks);
app.use('/api/v1/inspection', api.inspection);
app.use('/msiforks/', api.web)



// Rota para analisar as variaveis do sistema.
//app.get('/debugg/vars', function (req, res) { res.json(process.env); });


/*
Para rotas inexistentes, acessa esse endereço.
*/
app.get('*', function (req, resp) {
  resp.json({
    success: true
    , msg: "Servidor Online, desde " + onlinesince
  });
});
