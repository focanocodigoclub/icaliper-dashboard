(function (angular) {


	var app = angular.module('icaliper-server', ['ui.materialize'])
	app.controller('controller-list-inspection', ListInspection);


	/*
		!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			A MESMA LOGICA APLICADA AQUI FOI A UTILIZADA PARA O DESENVOLVIMENTO DO APLICATIVO
			PARA ALTERAR A LÓGICA REALIZADA AQUI, LEMBRE-SE DE ATUALIZAR A LÓGICA DO APLICATIVO
		!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

	*/


	function ListInspection($scope, $http) {

		$scope.q = {}
		$scope.showFilters = true;

		$scope.cleanFilter = function (enable) {
			if (!enable) {
				$scope.q = {};
			}
		}
		$scope.removeFinalResultQuery = function () {
			delete $scope.q["finalresult"];
		}
		$scope.removeLanguageQuery = function () {
			delete $scope.q["language_inspection"];
		}
		/*
		    Status
		    1 - Normal
		    2 - Trocar
		    3 - Consertar
		*/
		function ResultInspection(data) {
			if (!data.analysis_parts) {
				data.analysis_parts = [];
			}

			var step1 = step1Calcule(data.thickness_average, data.thickness);
			var step2 = step2Calcule(data.status_crack);
			var step3 = step3Calcule(data.length_average, data.length);
			var step4 = step4Calcule(data.hypotenuse_average);
			var step5 = step5Calcule(data.analysis_parts);

			if (step1 == 2 || step2 == 2 || step3 == 2 || step4 == 2 || step5 == 2) {
				return 2 // Trocar garfo
			}

			if (step3 == 3 || step4 == 3 || step5 == 3) {
				return 3 // Consertar Garfo
			}

			return 1 // Está tudo OK com o Garfo
		}

		function step1Calcule(ta, to) { return ta <= (to - (to * 0.1)) ? 1 : 2 }
		function step2Calcule(sc) { return sc ? 1 : 2; }
		function step3Calcule(la, lo) { return la < (lo * 0.03) ? 1 : 3 }
		function step4Calcule(hyp) { return hyp >= 560 && hyp <= 580 ? 1 : 3 }
		function step5Calcule(analysis_parts) {

			if (analysis_parts.indexOf('T') >= 0) { return 2; } // Trocar
			if (analysis_parts.indexOf('G') >= 0) { return 2; } // Trocar
			if (analysis_parts.indexOf('O') >= 0) { return 2; } // Trocar
			if (analysis_parts.indexOf('P') >= 0) { return 3; } // Concertar
			return 1
		}


		$scope.list = []

		$http({
			url: location.origin + '/api/v1/inspection/historic/list'
			, method: 'GET'
		}).then(function (result) {

			var data = result.data.data

			data.forEach(function (d) {
				d.finalresult = ResultInspection(d)
			})

			console.log(data)
			$scope.list = data;

		}, function (err) {

			console.log(err)

		})




		var currentTime = new Date();
		$scope.currentTime = currentTime;
		$scope.month = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'];
		$scope.monthShort = ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'];
		$scope.weekdaysFull = ['Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sabado', 'Domingo'];
		$scope.weekdaysLetter = ['D', 'S', 'T', 'Q', 'Q', 'S', 'S'];
		$scope.today = 'Hoje';
		$scope.clear = 'Limpar';
		$scope.close = 'Fechar';
		$scope.maxDate = (new Date()).toISOString();

		$scope.changePage = function (page) {

			page = 10 * page;
			page = page > $scope.list.length ? $scope.list.length : page;

			var i = (page - 10);
			for (; i < page; i++) {
				console.log(i)
			}
		}
	}

})(angular);
